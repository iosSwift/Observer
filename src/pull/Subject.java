package pull;

/**
 * @ClassName Subject.java
 * @Description	主题抽象类
 * @author Jon
 * @date 2018年5月21日 下午4:00:45
 */
public interface Subject {
	/**
	 * Description 注册观察者对象
	 * 
	 * @param o
	 */
	public void registerObserver(Observer o);

	/**
	 * Description 删除观察者对象
	 * 
	 * @param o
	 */
	public void removeObserver(Observer o);

	/**
	 * Description 通知观察者对象
	 */
	public void notifyObservers();
}

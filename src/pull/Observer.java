package pull;

/**
 * @ClassName Observer.java
 * @Description	观察者抽象类
 * @author Jon
 * @date 2018年5月21日 下午4:01:53
 */
public interface Observer {
	/**   
	 * Description  		
	 * @param subject  主题
	 */ 
	public void update(Subject subject);
}

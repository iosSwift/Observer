package pull;

/**
 * @ClassName TestMain.java
 * @Description	测试类
 * @author Jon
 * @date 2018年5月21日 下午4:27:08
 */
		public class TestMain {
			public static void main(String[] args) {
				WeatherData weatherData = new WeatherData();
				CurrentConditionsDisplay conditionsDisplay = new CurrentConditionsDisplay(weatherData);
				weatherData.setMeasurements(11.11F, 22.22F, 33.33F);
			}
		}

package pull;
import java.util.ArrayList;

/**
 * @ClassName WeatherData.java
 * @Description 主题实现类
 * @author Jon
 * @date 2018年5月21日 下午4:06:39
 */
public class WeatherData implements Subject {
	private ArrayList<Object> observers;
	private float temperature;
	private float humidity;
	private float pressure;

	public ArrayList<Object> getObservers() {
		return observers;
	}

	public void setObservers(ArrayList<Object> observers) {
		this.observers = observers;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}

	public float getPressure() {
		return pressure;
	}

	public void setPressure(float pressure) {
		this.pressure = pressure;
	}

	public WeatherData() {
		observers = new ArrayList<>();
	}

	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		int i = observers.indexOf(o);
		if (i >= 0) {
			observers.remove(i);
		}
	}

	@Override
	public void notifyObservers() {
		for (int i = 0; i < observers.size(); i++) {
			Observer observer = (Observer) observers.get(i);
			observer.update(this);
		}
	}

	/**
	 * Description 数据更新，通知观察者
	 */
	public void messurementschanged() {
		notifyObservers();
	}

	/**
	 * Description 更新数据
	 * @param temperature  	温度
	 * @param humidity  	湿度
	 * @param pressure		 气压
	 */
	public void setMeasurements(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		messurementschanged();
	}
}

package jdkobserver;

import java.util.Observable;
import java.util.Observer;

/**
 * @ClassName CurrentConditionsDisplay.java
 * @Description 
 * @author Jon
 * @date 2018年5月23日 下午1:32:11
 */
public class CurrentConditionsDisplay implements Observer {
	
	/**
	 * 构造观察者对象时将自己注册到主题中
	 * @param o		主题
	 */
	public CurrentConditionsDisplay(Observable o) {
		o.addObserver(this);
	}

	/**
	 *	观察者被通知后的操作
	 */
	@Override
	public void update(Observable o, Object arg) {
		WeatherData data = (WeatherData) o;
		System.out.println("温度" + data.getTemperature());
		System.out.println("湿度" + data.getHumidity());
		System.out.println("气压" + data.getPressure());
	}

}

package jdkobserver;

/**
 * @ClassName test.java
 * @Description 
 * @author Jon
 * @date 2018年5月23日 上午11:57:26
 */
public class test {

	public static void main(String[] args) {
		WeatherData weatherData = new WeatherData();
		CurrentConditionsDisplay current = new CurrentConditionsDisplay(weatherData);
		weatherData.setMeasurements(30.25F, 60.25F, 50.75F);
	}

}

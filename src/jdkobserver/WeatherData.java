package jdkobserver;

import java.util.Observable;

/**
 * @ClassName Watched.java
 * @Description 	java内置的Observable重做气象站
 * @author Jon
 * @date 2018年5月23日 下午1:19:05
 */
public class WeatherData extends Observable {
	private float temperature;
	private float humidity;
	private float pressure;
	
	public WeatherData() {
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}

	public float getPressure() {
		return pressure;
	}

	public void setPressure(float pressure) {
		this.pressure = pressure;
	}
	
	
	/**   
	 * Description	改变状态 ，通知观察者 		  
	 */ 
	public void measurementsChanged() {
		setChanged();
		notifyObservers();
	}
	
	/**   
	 * Description  		更新数据
	 * @param temperature	温度
	 * @param humidity		湿度
	 * @param pressure  	气压
	 */ 
	public void setMeasurements(float temperature, float humidity, float pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		measurementsChanged();
	}
}

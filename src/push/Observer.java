package push;

/**
 * @ClassName Observer.java
 * @Description	观察者抽象类
 * @author Jon
 * @date 2018年5月21日 下午4:01:53
 */
public interface Observer {
	/**
	 * Description 更新信息
	 * @param temp 		温度
	 * @param humidity	湿度
	 * @param pressure	气压
	 */
	public void update(float temp, float humidity, float pressure);
}

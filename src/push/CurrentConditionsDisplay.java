package push;

/**
 * @ClassName CurrentConditionsDisplay.java
 * @Description	观察者具体实现类
 * @author Jon
 * @date 2018年5月21日 下午4:20:46
 */
public class CurrentConditionsDisplay implements Observer, DisplayElement {
	private float temperature;
	private float humidity;
	private Subject weatherData;

	/**
	 * @param weatherData
	 *            创建自己时订阅主题
	 */
	public CurrentConditionsDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	@Override
	public void display() {
		System.out.println("温度:" + temperature + "\n湿度:" + humidity);
	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		this.temperature = temp;
		this.humidity = humidity;
		display();
	}
}

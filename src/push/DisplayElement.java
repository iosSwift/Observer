package push;

/**
 * @ClassName DisplayElement.java
 * @Description	展示信息公共接口
 * @author Jon
 * @date 2018年5月21日 下午4:05:43
 */
public interface DisplayElement {
	/**
	 * Description 展示信息
	 */
	public void display();
}
